#!/bin/bash

# Network "Diagnostics"
# This is a script I wrote to serve me with """Diagnostics"""
# By that I mean the current ping to a given website, as well as Up/Down speeds from speedtest-cli.
# Why am I making this? Because my Spanish Teacher would not believe us that our internet conditions are too bad to use BigBlueButton (OSS Virtual Classroom Software)

set -e

function main() {
	OutPutPath=$(./gum/gum input --placeholder "Input destination path")
	if [ ! $(which ping) ] && [ ! $(which python3) ]; then
		echo "Couldn't find ping and/or python3!"
		exit 1;
	fi
	echo "starting..."
	touch $OutPutPath/download-speed.txt
	touch $OutPutPath/upload-speed.txt
	touch $OutPutPath/outputping.txt
	ping -c 10 google.com > $OutPutPath/outputping.txt
	(python3 -m speedtest 2> /dev/null | grep -i "Download") > $OutPutPath/download-speed.txt
	(python3 -m speedtest 2> /dev/null | grep -i "Upload") > $OutPutPath/upload-speed.txt
	echo -e "==> All done!"
}

case $1 in
	*)
		main;;
esac
