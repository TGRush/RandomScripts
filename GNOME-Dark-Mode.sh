#!/bin/bash

prefersColorScheme="$(./gum/gum choose default prefer-dark)"

gsettings set org.gnome.desktop.interface color-scheme "$prefersColorScheme"
