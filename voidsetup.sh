#!/bin/bash

# Void Linux personal setup script
# This assumes you've already setup a network connection.

function main() {
	sudo xbps-install -Su
	sudo xbps-install -Sy dbus-elogind elogind NetworkManager emptty xorg git zsh kitty pipewire wireplumber xdg-desktop-portal xdg-desktop-portal-gtk xdg-desktop-portal-wlr Waybar nerd-fonts-ttf noto-fonts-ttf noto-fonts-ttf-extra noto-fonts-cjk noto-fonts-emoji void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree clipman xdg-user-dirs xdg-user-dirs-gtk zsh wofi
	sudo ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/
	sudo xbps-reconfigure -f fontconfig
	sudo rm /var/service/dhcpcd
	sudo ln -s /etc/sv/dbus /var/service
	sudo ln -s /etc/sv/NetworkManager /var/service
	sudo ln -s /etc/sv/elogind /var/service
	sudo ln -s /etc/sv/emptty /var/service
	echo "sleeping 15 seconds to wait for NetworkManager..."
	sleep 15s
	mkdir ~/git
	git clone --depth=1 https://codeberg.org/TGRush/dotfiles.git ~/git/dotfiles
	cp -r ~/git/dotfiles/kitty/kitty.conf ~/.config/kitty/kitty.conf
	cp -r ~/git/dotfiles/sway ~/.config/
	cp -r ~/git/dotfiles/waybar ~/.config/
	cp -r ~/git/dotfiles/wofi ~/.config/
	cp -r ~/git/dotfiles/emptty ~/.config/
	cp -r ~/git/dotfiles/dunst ~/.config/
	cp -r ~/git/dotfiles/micro ~/.config/
	cp -r ~/git/dotfiles/btop ~/.config/
	# ln -s /usr/share/applications/pipewire.desktop /etc/xdg/autostart/pipewire.desktop
	# ln -s /usr/share/applications/pipewire-pulse.desktop /etc/xdg/autostart/pipewire-pulse.desktop
	echo "Finished! Make sure to install a Desktop/WM next."
}

function novideo() {
	sudo xbps-install -Sy nvidia
	sudo xbps-install -Sy nvidia-libs-32bit
	echo "Nvidia Drivers Installed!"
}

case $@ in
	--nvidia)
		main
		novideo;;
	*)
		main;;
esac