#!/bin/bash

tmux new-session -d simple-proxy -port ${PORT}
tmux split-window -h ssh -R serveo.net:${PORT}:localhost:${PORT} serveo.net
# sends keys to first and second terminals
# tmux send -t 0:0.0 "simple-proxy -port 8888" C-m
# tmux send -t 0:0.1 "ssh -R serveo.net:8888:localhost:8888 serveo.net" C-m
tmux -2 attach-session -d
