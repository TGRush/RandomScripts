#!/bin/bash

# This is a re-write of the OpenAsar.sh script found in this repository.
# I am re-making it to allow for it to be used without any external dependencies other the obvious ones (Flatpak, a working shell, and a brain)
# It's mainly designed for Flatpak, and should work on the Steam Deck and immutable distros
# Additionally, it's a lot less code lol

set -e

# Ansi color code variables
# shellcheck disable=SC2034
red="\e[0;91m"
# shellcheck disable=SC2034
blue="\e[0;94m"
# shellcheck disable=SC2034
green="\e[0;92m"
# shellcheck disable=SC2034
white="\e[0;97m"
# shellcheck disable=SC2034
bold="\e[1m"
# shellcheck disable=SC2034
uline="\e[4m"
# shellcheck disable=SC2034
reset="\e[0m"

currentRelease="https://github.com/GooseMod/OpenAsar/releases/latest/download/app.asar"

function flatpakInstall() {
	echo -e "${green}==> Installing Discord with Flatpak${reset}"
	flatpak install com.discordapp.Discord
	wget "$currentRelease"
	sudo cp ./app.asar "$(flatpak info -l com.discordapp.Discord)/files/discord/resources/app.asar"
	if [ $SKIP_CLEAN == "true" ];then
		echo "not removing downloaded app.asar on user request"
	else
		rm ./app.asar
	fi
	echo -e "${green}==> All Done!${reset}"
}

function helpFunc() {
	echo -e "This script will assume you're using Flatpa by default, and currently does not support anything else."
	echo -e "Thus, the only valid arguments are:\n --help for help\n --flatpak for installing with Flatpak"
}

case $1 in
	--flatpak)
		flatpakInstall;;
	--help)
		helpFunc;;
	*)
		echo -e "${red}No Option Specified! Assuming Flatpak installation of Discord...${reset}"
		flatpakInstall;;
esac
