# This is just a couple of random scripts I make for myself out of convenience
## You can use these, though it'd probably be better if you did it yourself.

Either way,
have a nice day, and remember that you're loved :)

---

```bash
# Using OpenAsar-Reworked.sh (This assumes you have `curl` available.):
sh -c "$(curl -fsSL https://codeberg.org/TGRush/RandomScripts/raw/branch/master/OpenAsar-Reworked.sh)"
```
