#!/bin/bash

set -e

# Ansi color code variables
# shellcheck disable=SC2034
red="\e[0;91m"
# shellcheck disable=SC2034
blue="\e[0;94m"
# shellcheck disable=SC2034
green="\e[0;92m"
# shellcheck disable=SC2034
white="\e[0;97m"
# shellcheck disable=SC2034
bold="\e[1m"
# shellcheck disable=SC2034
uline="\e[4m"
# shellcheck disable=SC2034
reset="\e[0m"

# A simple little script I made, because I didn't want to install OpenAsar myself :)

currentRelease="https://github.com/GooseMod/OpenAsar/releases/latest/download/app.asar"

flatpakInstall() {
	if [ -d /home/$USER/.asarBackup ]; then
		echo -e "${green}App.asar backup already exists, skipping...${reset}"
	elif [ ! -d /home/$USER/.asarBackup ]; then
		echo -e "${green}Taking a backup of the original app.asar${reset}"
		mkdir -p /home/$USER/.asarBackup
		# flatpak info -l is used to get the installation path to Discord, the rest is added on-top as to define the app.asar location
		cp -r $(flatpak info -l com.discordapp.Discord)/files/discord/resources/app.asar /home/$USER/.asarBackup/app.asar
	else
		echo -e "${red}Couldnt determine if a backup exists!"
		exit 1
	fi
	flatpak install com.discordapp.Discord
    wget "$currentRelease" || (echo -e "${red}Something went wrong in the Download!${reset}" ; exit 1;)
    sudo mv app.asar $(flatpak info -l com.discordapp.Discord)/files/discord/resources/app.asar
    echo "==> All Done!"
    exit 0;
}

# deb, rpm, pacman, AUR, etc...
nativeInstall() {
    echo -e "${red}Not implemented!${reset}"
    exit 1;
}

restoreAsar() {
		if [ ! -d /home/$USER/.asarBackup ]; then
		echo -e "${green}No backup found!${reset}"
	elif [ -d /home/$USER/.asarBackup ]; then
		echo -e "${green}Loading backup of the original app.asar...${reset}"
		cp -r  /home/$USER/.asarBackup/app.asar $(flatpak info -l com.discordapp.Discord)/files/discord/resources/app.asar
	fi
}

case $1 in 
	--flatpak)
		flatpakInstall;;
esac

tuiinput=$(./gum/gum choose "Flatpak" "Native" "Restore")
	case $tuiinput in
		[fF]latpak)
			flatpakInstall
			;;
		[nN]ative)
			nativeInstall
			;;
		[rR]estore)
			restoreAsar
			;;	
	esac
